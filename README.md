# World Incarceration
World Incarceration allows users to explore incarceration statistics by country using ZingChart JS.

## Environment
[View the website here.](https://cleberg.io/world-incarceration/)

## Preview
![Preview Image](https://img.cleberg.io/share/world-incarceration.png)
